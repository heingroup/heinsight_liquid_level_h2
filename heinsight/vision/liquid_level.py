import atexit
import json
import warnings
from datetime import datetime
from pathlib import Path
from typing import List, Callable, Union, Dict

import cv2
import numpy as np
import pandas as pd
from hein_utilities.runnable import Runnable

from heinsight.heinsight_utilities import colour_palette
from heinsight.heinsight_utilities.reference import ReferenceManager, Reference
from heinsight.heinsight_utilities.temporal_data import TemporalData
from heinsight.vision_utilities.camera import Camera
from heinsight.vision_utilities.image_analysis import height_at_line, height_width
from heinsight.vision_utilities.roi import ROI, ROIManager


"""
General background for cv2 and numpy images:
If you call image.shape, you get back (height, width, channels), and if you want to access a pixel in the image,
you need to search it by image[height][row][channel]. If there is only one channel in the image there will
not be a channel specified. Height is equivalent to the row in an image, and width is equivalent to a column in an
image. 

For an image the top-left corner of the image is the origin (0,0), and the width of the image increases as you travel
towards the top-right corner of the image (0, width), and the height of the image increases as you travel towards the
bottom-left of the image (height, 0). 

This may be unintuitive to how we would perceive image height where we normally  think that the origin should be in 
the bottom left of an image. A convention used in this module, is 'level' is used for the liquid level where the 
image origin is the lower left corner of the image, and '_level' is used for when the origin is the top left corner 
of the image.

An effort is made to use 'level' for the majority of the data, especially data that a person will see, but '_level' 
is used for convenience during the image analysis portion of this module

Another point, is that the levels will be reported as normalized to the height of the image that was analyzed, 
instead of being the actual row number in an image. 

Example: if the level was found to be 10 pixels down from the top of an image of 100 pixel height, then _level 
would be 0.1. and level would be 0.9.
"""


_default_datetime = '%Y-%m-%d %H:%M:%S.%f'  # default string format for datetime package
_roi_colour = colour_palette._colour_9_bgr
_limit_line_colour = colour_palette._colour_2_bgr
_liquid_level_line_colour = colour_palette._colour_10_bgr

# todo move this to hein utilities
# n = datetime.now()
# s = str(n)
# s == datetime.strftime(n, _default_datetime)  # true
# n == datetime.strptime(s, _default_datetime)  # true


def draw_level(image,
               level,  # bottom left origin
               left=0,
               right=None,
               colour=None,
               thickness=2,
               ):
    """
    Helper function to draw a level on an image as a dashed line, between the passed in left and right points plus
    additional horizontal padding

    :param image: image to draw the level on
    :param level: liquid level where the origin of the level is the bottom left corner
    :param left: optional, is the left most that point in the image to start drawing the line
    :param right: optional, is the left most that point in the image to stop drawing the line
    :param colour: line colour
    :param thickness: line thickness
    :return: an image with the level drawn on it
    """
    clone = image.copy()
    if colour is None:
        colour = _liquid_level_line_colour
    # get parameters for drawing the line
    horizontal_padding = 20
    height, width = height_width(image=image)
    left = left - horizontal_padding
    left = left if left >= 0 else 0
    if right is None:
        right = width
    right = right + horizontal_padding
    right = right if right <= width else width
    dash_length = 7  # length of a single dash
    space_length = 7  # length of the space between dashes

    _level = LiquidLevelMonitor.convert_measurement(measurement=level)
    _level = int(_level * height)  # convert to row number with top left origin
    for _left in range(left, right, dash_length + space_length):
        # _left is the current left point to start a dash from to draw the entire dashed line
        left_point = (_left, _level)
        right_point = (_left + dash_length, _level)
        clone = cv2.line(img=clone,
                         pt1=left_point,
                         pt2=right_point,
                         color=colour,
                         thickness=thickness,
                         )
    return clone


def find_canny_parameters(image, sigma=0.33):
    """Find good parameters for Canny edge detection"""
    # compute the median of the single channel pixel intensities
    median = np.median(image)
    # find bounds for Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * median))
    upper = int(min(255, (1.0 + sigma) * median))
    return lower, upper


def image_process_v1(*images: np.ndarray) -> Union[np.ndarray, List[np.ndarray]]:
    """
    Original image processing algorithm to find and enhance horizontal lines in an image

    Process images (3 channel bgr) for the measure function into a black and white image (1 channel? double
    check) that should show where the liquid level/other horizontal lines in the image are. These horizontal
    features will be white pixels in the processed image

    :param images:
    :return: a list of processed images
    """
    morph_rect_kernel_size = (2, 1)  # width by height; filter spots that dont conform to this
    blur_kernal = (7, 7)
    processed_images = []
    for image in images:
        processed = image.copy()
        # grayscale
        processed = cv2.cvtColor(processed, cv2.COLOR_BGR2GRAY)
        # blur
        processed = cv2.GaussianBlur(processed, blur_kernal, 0)
        # apply histogram equalization - works pretty well getting rid of histogram equalization too
        clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(2, 2))
        processed = clahe.apply(processed)
        canny_threshold_1, canny_threshold_2 = find_canny_parameters(processed)
        # canny
        processed = cv2.Canny(processed, canny_threshold_1, canny_threshold_2)
        # remove noise
        processed = cv2.dilate(processed, None, iterations=1)
        processed = cv2.erode(processed, None, iterations=1)
        # create a horizontal structural element
        horizontal_structure = cv2.getStructuringElement(cv2.MORPH_RECT, morph_rect_kernel_size)
        # apply morphological opening operation to remove vertical lines
        processed = cv2.morphologyEx(processed, cv2.MORPH_OPEN, horizontal_structure)
        processed_images.append(processed)
    if len(processed_images) == 1:
        return processed_images[0]
    return processed_images


def measure_v1(image: np.ndarray,
               roi: ROI,
               ) -> int:
    """
    Original measuring algorithm to find the liquid level

    Count the number of white pixels in horizontal slices within a masked version of the image (the image should
    be processed to only be a black and white image at this point). Return the bottom most row number of the slice
    with the most white pixels was found (this should be the location where the liquid level is found) normalized by the
    height of the image. The origin of the measurement/image is the top left corner of the image

    :param image:
    :param roi:
    :return: the row (i.e. height) of where the liquid level is found in image, normalized by  the height of the
        image, where the TOP left corner of the image as the origin (so moving DOWN the image is increasing height)
    """
    slice_height = 4
    masked_image = roi.mask_image(image=image)
    start_x, start_y, roi_width, roi_height = roi.rectangle
    stop_x = start_x + roi_width
    stop_y = start_y + roi_height
    image_height, _ = height_width(image=image)
    white_pixel_count_df = pd.DataFrame(columns=['_level', 'count'])
    for slice_start_index in range(start_y, stop_y, slice_height):
        _level = slice_start_index / image_height
        slice_stop_index = slice_start_index + slice_height if slice_start_index + slice_height <= image_height else slice_height
        masked_image_slice = masked_image[slice_start_index:slice_stop_index, start_x:stop_x]
        count = np.count_nonzero(masked_image_slice)
        add_data = {'_level': _level,
                    'count': count}
        white_pixel_count_df = white_pixel_count_df.append(add_data, ignore_index=True)
    white_pixel_count_df = white_pixel_count_df.sort_values(by='count',
                                                            ascending=False)
    _level = white_pixel_count_df['_level'].to_list()[0]
    return _level


def measure_v2(image: np.ndarray,
               roi: ROI,
               ) -> int:
    """
    Original measuring algorithm modified to find the liquid level that tries to find the bottom of the liquid
    level

    Count the number of white pixels in horizontal slices within a masked version of the image (the image should
    be processed to only be a black and white image at this point). Return the bottom most row number of the slice
    with the most white pixels was found (this should be the location where the liquid level is found) normalized by the
    height of the image. Then look to see if there are any other slices in close proximity below the best slice,
    and choose that as the liquid level; this is in an attempt to find the bottom of a liquid level. The origin of
    the measurement/image is the top left corner of the image

    :param image:
    :param roi:
    :return: the row (i.e. height) of where the liquid level is found in image, normalized by  the height of the
        image, where the TOP left corner of the image as the origin (so moving DOWN the image is increasing height)
    """
    slice_height = 4
    masked_image = roi.mask_image(image=image)
    start_x, start_y, roi_width, roi_height = roi.rectangle
    stop_x = start_x + roi_width
    stop_y = start_y + roi_height
    image_height, _ = height_width(image=image)
    white_pixel_count_df = pd.DataFrame(columns=['_level', 'count'])
    for slice_start_index in range(start_y, stop_y, slice_height):
        _level = slice_start_index / image_height
        slice_stop_index = slice_start_index + slice_height if slice_start_index + slice_height <= image_height else \
            slice_height
        masked_image_slice = masked_image[slice_start_index:slice_stop_index, start_x:stop_x]
        count = np.count_nonzero(masked_image_slice)
        add_data = {'_level': _level,
                    'count': count}
        white_pixel_count_df = white_pixel_count_df.append(add_data, ignore_index=True)
    white_pixel_count_df = white_pixel_count_df.sort_values(by='count',
                                                            ascending=False)
    _level = white_pixel_count_df['_level'].to_list()[0]
    _level_count = white_pixel_count_df['count'].to_list()[0]
    _best_level = _level
    _best_level_count = _level_count

    # try to find the bottom of the liquid level, by checking if there is another _level that is above it but within
    # some limit
    limit_magic_number = 0.05
    _level_limit = _level + limit_magic_number
    count_minimum_percent = 0.2
    count_minimum = count_minimum_percent * _best_level_count

    # first only find _levels that are above the best _level
    filtered_df = white_pixel_count_df.loc[white_pixel_count_df['_level'] > _best_level]
    # then remove any slices that are out of the limit
    filtered_df = filtered_df.loc[_level_limit > filtered_df['_level']]
    # require a minimum count sort
    filtered_df = filtered_df.loc[filtered_df['count'] > count_minimum]
    # sort filtered values so the highest _level is at the top
    filtered_df = filtered_df.sort_values(by='_level',
                                          ascending=False)
    if len(filtered_df) > 0:
        _level = filtered_df['_level'].to_list()[0]
    return _level


def measure_v3(image: np.ndarray,
               roi: ROI,
               ) -> int:
    """
    Original measuring algorithm modified to find the liquid level that tries to find the top of the liquid
    level

    Count the number of white pixels in horizontal slices within a masked version of the image (the image should
    be processed to only be a black and white image at this point). Return the bottom most row number of the slice
    with the most white pixels was found (this should be the location where the liquid level is found) normalized by the
    height of the image. Then look to see if there are any other slices in close proximity above the best slice,
    and choose that as the liquid level; this is in an attempt to find the top of a liquid level. The origin of
    the measurement/image is the top left corner of the image

    :param image:
    :param roi:
    :return: the row (i.e. height) of where the liquid level is found in image, normalized by  the height of the
        image, where the TOP left corner of the image as the origin (so moving DOWN the image is increasing height)
    """
    slice_height = 4
    masked_image = roi.mask_image(image=image)
    start_x, start_y, roi_width, roi_height = roi.rectangle
    stop_x = start_x + roi_width
    stop_y = start_y + roi_height
    image_height, _ = height_width(image=image)
    white_pixel_count_df = pd.DataFrame(columns=['_level', 'count'])
    for slice_start_index in range(start_y, stop_y, slice_height):
        _level = slice_start_index / image_height
        slice_stop_index = slice_start_index + slice_height if slice_start_index + slice_height <= image_height else \
            slice_height
        masked_image_slice = masked_image[slice_start_index:slice_stop_index, start_x:stop_x]
        count = np.count_nonzero(masked_image_slice)
        add_data = {'_level': _level,
                    'count': count}
        white_pixel_count_df = white_pixel_count_df.append(add_data, ignore_index=True)
    white_pixel_count_df = white_pixel_count_df.sort_values(by='count',
                                                            ascending=False)
    _level = white_pixel_count_df['_level'].to_list()[0]
    _level_count = white_pixel_count_df['count'].to_list()[0]
    _best_level = _level
    _best_level_count = _level_count

    # try to find the top of the liquid level, by checking if there is another _level that is below it but within
    # some limit
    limit_magic_number = 0.05
    _level_limit = _level - limit_magic_number
    count_minimum_percent = 0.75
    count_minimum = count_minimum_percent * _best_level_count

    # first only find _levels that are below the best _level
    filtered_df = white_pixel_count_df.loc[white_pixel_count_df['_level'] < _best_level]
    # then remove any slices that are out of the limit
    filtered_df = filtered_df.loc[_level_limit < filtered_df['_level']]
    # require a minimum count sort
    filtered_df = filtered_df.loc[filtered_df['count'] > count_minimum]
    # sort filtered values so the lowest _level is at the top
    filtered_df = filtered_df.sort_values(by='_level',
                                          ascending=True)
    if len(filtered_df) > 0:
        _level = filtered_df['_level'].to_list()[0]
    return _level


def liquid_level_algorithm_v1_base(*images: np.ndarray,
                                   roi: ROI,
                                   image_process_fn: Callable = image_process_v1,
                                   measure_fn: Callable = measure_v1,
                                   ) -> Union[float, List[float]]:
    """
    Original liquid level algorithm sequence of imaging processing then measuring

    :param images:
    :param roi:
    :param image_process_fn, function do image processing
    :param measure_fn, function to get the measurement of the level
    :return: a list of _levels (i.e. height) of where the liquid level is found in the passed in images, normalized by
        the height of the image, where the TOP left corner of the image as the origin (so moving DOWN the image is
        increasing height). or only 1 if only 1 image is passed in
    """
    processed_images = image_process_fn(*images)
    if type(processed_images) != list:
        processed_images = [processed_images]
    _levels = []
    for image in processed_images:
        _level = measure_fn(image=image,
                            roi=roi,
                            )
        _levels.append(_level)
        _level = float(np.average(_levels))
    if type(_levels) is list:
        if len(_levels) == 1:
            return _levels[0]
    return _levels


def liquid_level_algorithm_v1_0(*images: np.ndarray,
                                roi: ROI
                                ) -> Union[float, List[float]]:
    """
    Original liquid level algorithm sequence

    :param images:
    :param roi:
    :return: a list of _levels (i.e. height) of where the liquid level is found in the passed in images, normalized by
        the height of the image, where the TOP left corner of the image as the origin (so moving DOWN the image is
        increasing height). or only 1 if only 1 image is passed in
    """
    _levels = liquid_level_algorithm_v1_base(*images,
                                             roi=roi,
                                             image_process_fn=image_process_v1,
                                             measure_fn=measure_v1)
    return _levels


def liquid_level_algorithm_v1_1(*images: np.ndarray,
                                roi: ROI,
                                ) -> Union[float, List[float]]:
    """
    Original liquid level algorithm sequence modified to try to find the bottom of the liquid level


    :param images:
    :param roi:
    :return: a list of _levels (i.e. height) of where the liquid level is found in the passed in images, normalized by
        the height of the image, where the TOP left corner of the image as the origin (so moving DOWN the image is
        increasing height). or only 1 if only 1 image is passed in
    """
    _levels = liquid_level_algorithm_v1_base(*images,
                                             roi=roi,
                                             image_process_fn=image_process_v1,
                                             measure_fn=measure_v2)
    return _levels


def liquid_level_algorithm_v1_2(*images: np.ndarray,
                                roi: ROI,
                                ) -> Union[float, List[float]]:
    """
    Original liquid level algorithm sequence modified to try to find the top of the liquid level


    :param images:
    :param roi:
    :return: a list of _levels (i.e. height) of where the liquid level is found in the passed in images, normalized by
        the height of the image, where the TOP left corner of the image as the origin (so moving DOWN the image is
        increasing height)
    """
    _levels = liquid_level_algorithm_v1_base(*images,
                                             roi=roi,
                                             image_process_fn=image_process_v1,
                                             measure_fn=measure_v3)
    return _levels


def liquid_level(*images: np.ndarray,
                 roi: ROI,
                 algorithm: Callable) -> Union[float, List[float]]:
    """

    :param images:
    :param roi:
    :param algorithm, function to get the measurement of the level
    :return: list of rows (i.e. height) of where the liquid level is found in the passed in images, normalized by
        the height of the image, where the TOP left corner of the image as the origin (so moving DOWN the image is
        increasing height). or just one row, if only one is returned
    """
    _level = algorithm(*images,
                       roi=roi)
    return _level


def liquid_level_algorithm_ffd(*images,
                               roi: ROI):  # need 2+ images, and average result
    # todo liquid level algorithm
    raise NotImplementedError


class LiquidLevelMonitor:
    def __init__(self,
                 save_path: Path = Path.cwd().joinpath('liquid level data'),
                 algorithm: Callable = liquid_level_algorithm_v1_1,
                 ):
        """

        :param save_path: path to a save the data as a csv and json files to
        :param algorithm: an algorithm to measure the liquid level
        """
        self._algorithm = algorithm
        self._roi_manager = ROIManager()
        self._reference_manager = ReferenceManager()
        self._select_height: int = None
        self._select_width: int = None
        self._datetime_format = '%Y_%m_%d_%H_%M_%S_%f'  # string format for tracking time points for measurements
        self._data = TemporalData(save_path=save_path)
        self.data.datetime_format = self.datetime_format
        if save_path.is_dir():
            save_path = save_path.joinpath('liquid_level_data')
        self.save_path: Path = save_path
        self.set_up_data()

    @property
    def algorithm(self) -> Callable:
        return self._algorithm

    @algorithm.setter
    def algorithm(self,
                  value,
                  ):
        self._algorithm = value

    @property
    def roi_manager(self) -> ROIManager:
        return self._roi_manager

    @property
    def reference_manager(self):
        return self._reference_manager

    @reference_manager.setter
    def reference_manager(self, value):
        self._reference_manager = value

    @property
    def select_height(self) -> int:
        """height of the image used to select the roi(s)"""
        return self._select_height

    @select_height.setter
    def select_height(self,
                      value: int):
        self._select_height = value

    @property
    def select_width(self) -> int:
        """width of the image used to select the roi(s)"""
        return self._select_width

    @select_width.setter
    def select_width(self,
                     value: int):
        self._select_width = value

    @property
    def columns(self) -> List[str]:
        return self.data.columns

    @property
    def data(self) -> TemporalData:
        return self._data

    @data.setter
    def data(self,
             value: TemporalData):
        self._data = value

    @property
    def datetime_format(self) -> str:
        return self._datetime_format

    @datetime_format.setter
    def datetime_format(self,
                        value: str):
        self._datetime_format = value
        self.data.datetime_format = value

    @property
    def save_path(self) -> Path:
        return self._save_path

    @save_path.setter
    def save_path(self,
                  value: Path):
        self._save_path = value
        self.data.save_path = value

    @property
    def csv_path(self) -> Path:
        file_name = self.save_path.name
        return self.save_path.with_name(f'{file_name}.csv')

    @property
    def json_path(self) -> Path:
        file_name = self.save_path.name
        return self.save_path.with_name(f'{file_name}.json')

    def set_up_data(self):
        self.data.add_data({})

    def save_data(self):
        """Save a json file of this object without liquid level data, and save a csv file of only the liquid level
        data"""
        self.save_json()
        self.save_csv()

    def save_json(self, file_path=None):
        """Save all the data for this object into a json file except for the liquid level data"""
        data = {'roi_manager': self.roi_manager.data,
                'reference_manager': self.reference_manager.data,
                'select_height': self.select_height,
                'select_width': self.select_width,
                'datetime_format': self.datetime_format,
                'save_path': str(self.save_path)}
        if file_path is None:
            file_path = self.json_path
        try:
            with open(file_path, 'w') as file:
                json.dump(data, file)
        except PermissionError as e:
            pass

    def save_csv(self, file_path: str = None):
        """Save the liquid level data into a csv file"""
        self.data.save_csv(file_path)

    def load_data(self, file_path):
        """Load an instance of this object that was saved as a json file using the save_json function. Everything
        is loaded except for liquid level data as that is not saved in the save_json function"""
        with open(file_path) as file:
            data = json.load(file)
        self.roi_manager.data = data['roi_manager']
        self.reference_manager.data = data['reference_manager']
        self.select_height = data['select_height']
        self.select_width = data['select_width']
        self.datetime_format = data['datetime_format']
        self.save_path = Path(data['save_path'])

    def describe(self):
        """report back stats about all measurements"""
        raise NotImplementedError

    def latest_measurement(self) -> float:
        # todo pass
        raise NotImplementedError

    def measure(self,
                *images: np.ndarray,
                roi: Union[ROI, str],
                ) -> float:
        """
        Measure the average liquid level in images without updating self.data, for a specific ROI

        :param images:
        :param roi: either an ROI instance, or a string of the name of the roi

        :return: the average row (i.e. height) of where the liquid level is found in the passed in images, normalized by
            the height of the image, where the BOTTOM left corner of the image as the origin (so moving UP the
            image is increasing height) for each roi in a dictionary, where the key is the name of the roi

        """
        if type(roi) is str:
            name = roi
            roi = self.roi_manager.roi(name)
            if roi is None:
                raise KeyError(f"{name} is not a valid roi")
        descriptive_measurement = self.descriptive_measure(*images,
                                                           roi=roi)
        mean_level = descriptive_measurement["Mean"]
        return mean_level

    def descriptive_measure(self,
                            *images: np.ndarray,
                            roi: Union[ROI, str],
                            ) -> Dict[str, Union[List[float], float]]:
        """
        Measure the liquid level in images without updating self.data, for a specific ROI, and calculate additional
        stats.

        :param images:
        :param roi: either an ROI instance, or a string of the name of the roi

        :return: dictionary that describes the liquid level measurement made for the roi
        """
        if type(roi) is str:
            name = roi
            roi = self.roi_manager.roi(name)
            if roi is None:
                raise KeyError(f"{name} is not a valid roi")
        _levels = liquid_level(*images,
                               roi=roi,
                               algorithm=self.algorithm)    # origin in top left corner of an image
        if type(_levels) is float:
            levels = self.convert_measurement(_levels)  # origin in bottom left corner of an image
            mean = levels
            std_dev = None
            variance = None
            minimum = None
            maximum = None
            count = 1
        else:  # type is list
            levels = list(map(self.convert_measurement, _levels))  # origin in bottom left corner of an image
            mean = np.mean(levels)
            mean = round(mean, 3)
            # todo ddof = 0 to calculate the biased estimator, use ddof = 1 to calculate the unbiased estimator; use 0 if
            #  working with an entire distribution or population, use 1 if using a subset of values in the
            #  distribution or population
            std_dev = np.std(levels, ddof=0)
            variance = np.var(levels, ddof=0)
            minimum = min(levels)
            maximum = max(levels)
            count = len(levels)
        data = {"Levels": levels,
                'Count': count,
                'Mean': mean,
                'Standard deviation': std_dev,
                'Variance': variance,
                'Min': minimum,
                'Max': maximum,
                }
        return data

    def add_measurement(self,
                        *images: np.ndarray,
                        roi: Union[ROI, str, List[ROI], List[str]] = None,
                        t: str = None,
                        units: str = None,
                        ) -> Dict[str, Union[str, float]]:
        """
        Add the average level and additional descriptive stats of where the liquid level was found in images for each
        roi specified into self.data at a specific time. The liquid level is normalized by the height of the image,
        where the origin is in the bottom left corner of an image. The average liquid level of all the images is
        calculated

        To access the liquid level in the returned dictionary for a specific roi, use the name of the roi as the key

        If t is None, then the time is the current time
        If t is a string, use that as it is; note that it should be in the same datetime format as this object's
        datetime_format property
        If t is given as a datetime object, it is formatted into a string based on the object's datetime_format property
        If t is a float, then units must be given. In this case, t is the number of units since the previous time
        point. The other time columns will be calculated accordingly. If this data will be the first row in
        the object's data property (if it is the first piece of data added), then the current time is used as the
        time point although

        :param images:
        :param roi: either a/a list of ROI instances, or a/list of strings of the name of the roi. If left as none,
            measurements will be made in all rois
        :param t: time
        :param units: if t is a float, units is the units for the time since the last time point. See TemporalData
            add_data function for details on what units must be
        :return: the average row (i.e. height) of where the liquid level is found in the passed in images, normalized by
            the height of the image, where the BOTTOM left corner of the image as the origin (so moving UP the
            image is increasing height) for each roi in a dictionary, where the key is the name of the roi
        """
        # todo, maybe make measurement and save data for all rois, but only return a dictionary for the rois specified?
        rois: List[ROI] = []
        if roi is not None:
            if type(roi) is str:
                name = roi
                roi = self.roi_manager.roi(name)
                if roi is None:
                    raise KeyError(f"{name} is not a valid roi")
                rois.append(roi)
            elif type(roi) is ROI:
                rois.append(roi)
            elif type(roi) == list:
                if type(roi[0]) == str:
                    for name in roi:
                        r = self.roi_manager.roi(name)  # the actual roi for the roi name
                        if r is None:
                            raise KeyError(f"{name} is not a valid roi")
                        rois.append(r)
                if type(roi[0]) == ROI:
                    for r in roi:
                        rois.append(r)
        else:
            rois = list(self.roi_manager.rois.values())

        count = 0
        add_data = {}
        for roi in rois:
            descriptive_measurement = self.descriptive_measure(*images,
                                                               roi=roi)
            level = descriptive_measurement["Mean"]
            std_dev = descriptive_measurement["Standard deviation"]
            minimum = descriptive_measurement["Min"]
            maximum = descriptive_measurement["Max"]
            roi_name = roi.name
            add_data[roi_name] = level
            add_data[roi_name + ' standard deviation'] = std_dev
            add_data[roi_name + ' min'] = minimum
            add_data[roi_name + ' max'] = maximum
            count = descriptive_measurement["Count"]
        add_data['Count'] = count
        self.data.add_data(add_data, t, units)
        return add_data

    def descriptive_good(self,
                         *images,
                         roi: Union[str, List[str]] = None,
                         t: str = None,
                         units: str = None,
                         ) -> Dict[str, Union[str, float, bool]]:
        """
        Check whether for the image(s), if the average liquid level found within the rois specified, is within the
        reference associated with the roi or not.

        Return the dictionary returned by the add_measurement method with additional '{roi name} good' as keys
        for each roi specified, where the values are bools of if the average liquid level from the images is within
        the reference or not. Update data property with this dictionary

        If t is None, then the time is the current time
        If t is a string, use that as it is; note that it should be in the same datetime format as this object's
        datetime_format property
        If t is given as a datetime object, it is formatted into a string based on the object's datetime_format property
        If t is a float, then units must be given. In this case, t is the number of units since the previous time
        point. The other time columns will be calculated accordingly. If this data will be the first row in
        the object's data property (if it is the first piece of data added), then the current time is used as the
        time point although

        :param images:
        :param roi:
        :param t: time
        :param units: if t is a float, units is the units for the time since the last time point. See TemporalData
            add_data function for details on what units must be

        :return:
        """
        data = self.add_measurement(*images, roi=roi)
        self.data.drop_tail(1)  # drop the last row of data since the add measurement function appends to the data
        # property
        if roi is None:
            roi = self.roi_manager.names
        if type(roi) == str:
            roi = [roi]
        for name in roi:
            level = data[name]
            if self.reference_manager.reference(name) is not None:
                good: bool = self.reference_manager.good(level, reference_name=name)
            else:
                good = None
            data[f'{name} good'] = good
        self.data.add_data(data, t, units)
        return data

    def good(self,
             *images,
             roi: Union[str, List[str]] = None,
             ) -> Union[bool, Dict[str, bool]]:
        """
        Check whether for the image(s), if the average liquid level found within the rois specified, is within the
        reference associated with the roi or not

        If more than one roi is specified, return a dictionary with roi names are keys keys, and the values
        are bools of if the average liquid level from the images is within the reference for that roi or not.
        If only one roi is specified, return just a bool

        :param images:
        :param roi: one or more strings of roi names
        :return:
        """
        if roi is None:
            roi = self.roi_manager.names
        if type(roi) == str:
            roi = [roi]
        good_dict = {}
        data = self.descriptive_good(*images, roi=roi)
        for name in roi:
            good: bool = data[f'{name} good']
            good_dict[name] = good

        if len(roi) == 1:
            return good_dict.popitem()[1]
        else:
            return good_dict

    def head(self,
             n: int,
             column: str = None) -> Union[pd.DataFrame, pd.Series]:
        """
        Return the first n rows of data

        :param n:
        :param column: str, column name
        :return:
        """
        data = self.data.head(n, column)
        return data

    def tail(self,
             n: int,
             column: str = None) -> Union[pd.DataFrame, pd.Series]:
        """
        Return the last n rows of data

        :param n:
        :param column: str, column name
        :return:
        """
        data = self.data.tail(n, column)
        return data

    def drop_tail(self,
                  n,
                  ):
        """Drop the last n rows of data"""
        self.data.drop_tail(n)

    @staticmethod
    def convert_measurement(measurement: float) -> float:
        """
        Convert a liquid level measurement between having the measurement origin be in the top left corner of an
        image (standard for images in Python) and an origin in the bottom left corner of an image (human intuitive,
        because now a larger measurement is higher up/near the top of an image)

        :param measurement: liquid level measurement, where the origin is either in the top left or top right corner
            of an image
        :return: liquid level measurement, where the origin is the opposite of the input measurement
        """
        return 1 - measurement

    def add_roi(self,
                image,
                roi_type: str,
                name: str,
                window_name: str = None,
                ) -> ROI:
        """

        :param ROI, roi: an roi to add
        :param str, roi_type: one of roi_types to create and add, one of roi_types
        :param str, name: name to give an roi to create and add
        :param image:
        :param window_name: str to give specific instructions to select the roi
        :return:
        """
        roi = self.roi_manager.add_roi(roi_type=roi_type,
                                       name=name,
                                       image=image,
                                       window_name=window_name,
                                       )
        self.select_height = roi.select_height
        self.select_width = roi.select_width
        return roi

    def remove_roi(self, name):
        if name not in self.roi_manager.names:
            return
        self.roi_manager.remove_rois(name)
        self.reference_manager.remove_reference(name)

    def add_reference(self,
                      image: np.ndarray,
                      name: str,
                      upper: bool = True,
                      lower: bool = True,
                      ) -> Reference:
        """
        A reference must be associated to an roi; an roi can only be associated with 1 reference

        :param image:
        :param name: str, there must be a valid corresponding roi with the same name for the reference to be
            associated with
        :param upper: true to set an upper limit
        :param lower: true to set a lower limit
        :return:
        """
        image_height, image_width = height_width(image)
        if name not in self.roi_manager.names:
            raise KeyError(f'{name} is not a valid roi')
        # clone the image to be able to draw the roi on it, and the upper level on the clone if both the upper and
        # lower level are to be selected
        clone = image.copy()
        clone = self.draw_roi(clone, name)
        if upper is True and lower is True:
            window_name = 'Select upper limit, press "space bar" to select, "r" to reset selections, and "q" to exit'
            upper_level = height_at_line(clone, window_name)
            # convert to row number with top left origin
            _upper_level = LiquidLevelMonitor.convert_measurement(measurement=upper_level)
            _upper_level = int(_upper_level * image_height)
            # draw the first line on the image so you know where to add the second line
            clone = cv2.line(img=clone,
                             pt1=(0, _upper_level),
                             pt2=(image_width, _upper_level),
                             color=_limit_line_colour,
                             thickness=2,
                             )
            window_name = 'Select lower limit, press "space bar" to select, "r" to reset selections, and "q" to exit'
            lower_level = height_at_line(clone, window_name)
            difference = upper_level - lower_level
            half_difference = difference / 2
            middle = lower_level + half_difference
            reference = self.reference_manager.add_reference(middle, half_difference, half_difference, name)
        elif upper is True and lower is False:
            window_name = 'Select upper limit, press "space bar" to select, "r" to reset selections, and "q" to exit'
            upper_level = height_at_line(clone, window_name)
            reference = self.reference_manager.add_reference(upper_level, upper_level, 0, name)
        elif lower is True and upper is False:
            window_name = 'Select lower limit, press "space bar" to select, "r" to reset selections, and "q" to exit'
            lower_level = height_at_line(clone, window_name)
            reference = self.reference_manager.add_reference(lower_level, 0, 1 - lower_level, name)
        else:
            raise ValueError('At least one of upper or lower must be True')
        return reference

    def remove_reference(self, name):
        if name not in self.reference_manager.names:
            return
        self.reference_manager.remove_reference(name)

    def draw_roi(self,
                 image: np.ndarray,
                 roi: Union[str, List[str]] = None,
                 annotate: bool = False,
                 ) -> np.ndarray:
        if roi is None:
            roi = self.roi_manager.names
        if type(roi) == str:
            roi = [roi]
        for name in roi:
            image = self._draw_roi(image, name, annotate)
        return image

    def _draw_roi(self, image, name: str, annotate: bool):
        image_copy = image.copy()
        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 1
        roi = self.roi_manager.roi(name)
        if roi.points is not None:  # roi has been set
            image_copy = roi.roi(image_copy)
            if annotate is True:
                (x, y, width, height) = roi.rectangle
                middle = int(x + (width / 2))
                text_position = (middle, y - 20)
                cv2.putText(image_copy, name, text_position, font, font_scale, _roi_colour)
        return image_copy

    def draw_reference(self,
                       image: np.ndarray,
                       reference: Union[str, List[str]] = None,
                       ) -> np.ndarray:
        """Draw the upper and lower limits for a reference/list of references on an image"""
        if reference is None:
            reference = self.reference_manager.names
        if type(reference) == str:
            reference = [reference]
        for name in reference:
            image = self._draw_reference(image, name)
        return image

    def _draw_reference(self, image, name: str):
        # todo extract main drawing functionality into reference class
        padding = 20  # extra horizontal padding to make the reference lines go to
        image_copy = image.copy()
        image_height, image_width = height_width(image)
        thickness = 2
        roi = self.roi_manager.roi(name)  # the associated roi
        (x, y, width, height) = roi.rectangle
        left = x - padding
        left = left if left >= 0 else 0
        right = x + width + padding
        right = right if right <= image_width else image_width

        reference = self.reference_manager.reference(name)
        # draw function uses top left corner as image origin, so need to convert the relative upper and lower limits
        # to absolute pixel values and do 1-upper and 1-lower to convert to use the top left corner as the image origin
        upper = int((1 - reference.upper) * image_height)
        upper = upper if upper >= y else y
        lower = int((1 - reference.lower) * image_height)
        lower = lower if lower <= y + height else y + height
        cv2.line(image_copy,
                 (left, upper),
                 (right, upper),
                 _limit_line_colour,
                 thickness
                 )
        cv2.line(image_copy,
                 (left, lower),
                 (right, lower),
                 _limit_line_colour,
                 thickness
                 )
        return image_copy


class LiquidLevelMonitorRunnable(Runnable):
    def __init__(self,
                 camera: Camera,
                 monitor: LiquidLevelMonitor,
                 save_path: Path = Path.cwd().joinpath('liquid level'),
                 n_images: int = 1,
                 name: str = '',
                 ):
        """
        Set camera path to also save a video of the raw camera footage

        :param monitor:
        :param camera:
        :param save_path: base path to save videos (additional text will be appended for different video types); if
            a directory is passed in, the videos will be saved in that directory with the same base name as the
            directory
        :param n_images: number of images to use to get a liquid level measurement
        :param name:
        """
        warnings.warn(
            'name parameter no longer used',
            FutureWarning,
            stacklevel=2,
        )
        # todo
        Runnable.__init__(self)
        self.monitor = monitor
        self.camera = camera
        self.n_images = n_images  # n images used to get a single level measurement
        frame_width = monitor.select_width
        frame_height = monitor.select_height
        if save_path.is_dir():
            save_path = save_path.joinpath(save_path.name)
        self.save_video: bool = True  # whether to save videos of the liquid level analysis in real time or not
        self.camera_video_path: str = None  # needs to be set for the raw camera stream to be saved
        self.save_photos: bool = False  # whether or not to save all the raw images taken by the camera for
        # video recording or not
        self.drawn_images_path = save_path.with_name('drawn images')  # folder to save all images (annotated)
        name = save_path.name
        level_video_path = save_path.with_name(f'{name}.mp4')
        level_video_path = str(level_video_path)
        roi_video_path = save_path.with_name(f'{name} detailed.mp4')
        roi_video_path = str(roi_video_path)
        process_video_path = save_path.with_name(f'{name} process.mp4')
        process_video_path = str(process_video_path)
        fourcc = 0x00000021  # mp4
        self.level_writer = cv2.VideoWriter(level_video_path,
                                            fourcc,
                                            30,
                                            (frame_width, frame_height), True)
        self.detailed_writer = cv2.VideoWriter(roi_video_path,
                                               fourcc,
                                               30,
                                               (frame_width, frame_height), True)
        self.processed_writer = cv2.VideoWriter(process_video_path,
                                                fourcc,
                                                30,
                                                (frame_width, frame_height), True)
        atexit.register(self.stop_monitoring)

    def run(self):
        images = []
        while self.running:
            if self.camera.running is False:
                self.camera.start_recording(self.camera_video_path, self.save_photos)
            image = self.camera.last_frame
            if image is None:
                continue
            images.append(image)
            # when n images have been captured
            if len(images) == self.n_images:
                # dictionary, where keys are the names of each roi, and the value is a dictionary that contains the
                # liquid level found in the roi and the left and right edges of the roi. this information is for
                # drawing the annotation on the images
                drawing_info = {}
                # find the level for each roi
                level_dict = self.monitor.descriptive_good(*images)
                # preparing frames to and writing them to save a video of the run
                # for each roi
                for roi in list(self.monitor.roi_manager.rois.values()):
                    roi_name = roi.name
                    level = level_dict[roi_name]  # average level for all images in that roi
                    (x, _, width, _) = roi.rectangle  # find the rectangle that encloses the roi
                    left = x
                    right = x + width
                    drawing_info[roi_name] = {'level': level, 'left': left, 'right': right}
                # for each image draw the level fir each roi on the image
                for image in images:
                    # draw all levels on the image
                    for roi_name in drawing_info:
                        roi_info = drawing_info[roi_name]
                        image = draw_level(image=image,
                                           level=roi_info['level'],
                                           left=roi_info['left'],
                                           right=roi_info['right'],
                                           )
                    # annotate the image
                    detailed_image = self.monitor.roi_manager.rois_image(image)
                    detailed_image = self.monitor.draw_reference(detailed_image)
                    # save the detailed image
                    now = datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f')
                    cv2.imwrite(str(self.drawn_images_path.joinpath(f'{now}.png')), detailed_image)
                    # if immediately creating a movie, write the frame
                    if self.save_video is True:
                        self.level_writer.write(image)
                        self.detailed_writer.write(detailed_image)
                        processed_image = image_process_v1(image)
                        self.processed_writer.write(processed_image)
                images = []

    def release_video_writers(self):
        self.level_writer.release()
        self.detailed_writer.release()
        self.processed_writer.release()

    def start_monitoring(self, video_path: str = None):
        self.drawn_images_path.mkdir()
        if video_path is not None:
            self.camera_video_path = video_path
        self.start()

    def stop_monitoring(self):
        self.camera.stop_recording()
        self.stop()
        self.release_video_writers()

    def stream(self) -> None:
        """
        Stream a live view of the camera with the identified liquid level drawn on the stream.
        Press the 'q' button to exit out of the stream
        :return:
        """
        self.camera.disconnect()
        # stream what the video sees
        video_capture = cv2.VideoCapture(self.camera.port, cv2.CAP_DSHOW)
        cv2.namedWindow("Stream drawn - press 'q' button to exit", cv2.WINDOW_NORMAL)
        while True:
            # if press the q button exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            # capture frame-by-frame
            _, frame = video_capture.read()
            # dictionary, where keys are the names of each roi, and the value is a dictionary that contains the
            # liquid level found in the roi and the left and right edges of the roi. this information is for
            # drawing the annotation on the images
            drawing_info = {}
            # for each roi
            for roi in list(self.monitor.roi_manager.rois.values()):
                roi_name = roi.name
                # find the level for the roi
                level = self.monitor.measure(frame, roi=roi_name)
                (x, _, width, _) = roi.rectangle  # find the rectangle that encloses the roi
                left = x
                right = x + width
                drawing_info[roi_name] = {'level': level, 'left': left, 'right': right}
            # draw all levels on the image
            for roi_name in drawing_info:
                # annotate the image with the reference and roi
                frame = self.monitor.roi_manager.rois_image(frame)
                frame = self.monitor.draw_reference(frame)
                roi_info = drawing_info[roi_name]
                frame = draw_level(image=frame,
                                   level=roi_info['level'],
                                   left=roi_info['left'],
                                   right=roi_info['right'],
                                   )
                cv2.imshow("Stream drawn - press 'q' button to exit", frame)
        video_capture.release()
        cv2.destroyAllWindows()
        self.camera.connect()

