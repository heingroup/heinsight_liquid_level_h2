"""
Image analysis to contain methods used to do simple image analysis on images, and to prepare images for analysis.
Example of preparing an image for analysis is letting the user select an area in an image, and returning that area so
other analyses can be done for just that area
"""

import cv2
import numpy as np
import imutils
from typing import Union, List
from matplotlib import pyplot as plt
import warnings


def height_at_line(image: np.ndarray,
                   window_name: str = None,
                   ):
    """
    Function to allow the user to select a single line on an image, and be returned the height of the line normalized
    by the height of the image, using the bottom left corner of the image as the origin. Press 'space bar' to finalize
    selection of the line, press 'q' to exit. Height is based on the image origin bein gin the bottom left corner of
    the image, and the height is normalized by the image height

    :param image:
    :param str, window_name:

    :return: float, height of the line normalized by the height of the image using the bottom left corner of the
        image as the image origin
    """
    image = image.copy()
    clone = image.copy()
    points_from_selection = []  # list of points from when the user left clicks on the displayed image
    image_height, image_width = height_width(image=image)
    if window_name is None:
        window_name = 'Select line, press "space bar" to select, "r" to reset selections, and "q" to exit'
    def make_selection(event, x, y, flags, param):
        # if left mouse button clicked, record the starting(x, y) coordinates
        if event is cv2.EVENT_LBUTTONDOWN:
            points_from_selection.append((x, y))
            # draw line from the point selected
            line_left = (0, y)
            line_right = (image_width, y)
            green = (0, 255, 0)
            cv2.line(image, line_left, line_right, green, 2)
            cv2.imshow(window_name, image)
    cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(winname=window_name, width=image_width, height=image_height)
    cv2.setMouseCallback(window_name, make_selection)
    # keep looping until 'q' is pressed
    while True:
        # display image, wait for a keypress
        cv2.imshow(window_name, image)
        key = cv2.waitKey(1) & 0xFF
        # if 'r' key is pressed, reset selections
        if key == ord('r'):
            points_from_selection = []
            image = clone.copy()
        # if 'space bar' key pressed break from while True loop
        if key == ord(' '):
            # if there was a single selection made then return the absolute height of the selected point from the
            # image
            if len(points_from_selection) == 1:
                _line_height = points_from_selection[0][1]  # top left corner is the origin
                line_height = (image_height - _line_height)/image_height  # bottom left corner is the origin and
                # normalize to image height
                cv2.waitKey(0)
                cv2.destroyAllWindows()
                return line_height
            else:
                print('You must only select a single level. Press "r" to reset line selection')


def height_width(image):
    """
    Find the height and width of an image, whether it is a grey scale image or not

    :param image: an image, as a numpy array
    :return: int, int: the height and width of an image
    """
    if len(image.shape) is 3:
        image_height, image_width, _ = image.shape
    elif len(image.shape) is 2:
        image_height, image_width = image.shape
    else:
        raise ValueError('Image must be passed as a numpy array and have either 3 or 2 channels')

    return image_height, image_width


def display(image: np.ndarray,
            window_name: str = 'Image',
            ):
    """
    Display a cv2 image. User needs to press any key before anything else will happen. Image will stop being
    displayed when user exits out of the image window

    :param image:
    :param str, window_name:

    :return:
    """
    cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
    height, width = height_width(image=image)
    cv2.resizeWindow(winname=window_name, width=width, height=height)
    cv2.imshow(winname=window_name, mat=image)
    cv2.waitKeyEx(0)


def draw_line(image, left_point, right_point, colour=None, thickness=None):
    """
    Helper function to draw a single line on an image. image origin is the top right corner

    :param image: image to draw the line on
    :param (int, int), left_point: the left point of the line, as (width, height) or equivalently (column, row)
    :param (int, int), right_point: the right point of the line, as (width, height) or equivalently (column, row)
    :param (int, int, int), colour: colour of the line in (b, g, r)
    :param int, thickness: line thickness
    :return: image with line and text drawn on the image
    """
    warnings.warn(
        'use cv2 line method directly',
        DeprecationWarning,
        stacklevel=2,
    )
    image = cv2.line(image,
                     left_point,
                     right_point,
                     colour,
                     thickness)
    return image


def histogram(image,
              channel=0,
              ):
    """
    Get a histogram of the image; histogram of the number of pixels with a given intensity.

    :param numpy.ndarray, image: image
    :param int, channel: either 0 or 3, 0 if image is greyscale, 3 if it is RGB or BGR or hsv
    :return: numpy.ndarray, hist: 256 by 1 array, each value corresponds to the number of pixels in
        the image with its corresponding pixel value
    """
    hist = cv2.calcHist(
        [image],
        [channel],
        None,
        [256],
        [0, 256]
    )
    return hist


def sum_histogram(hist: np.ndarray,
                  ):
    """
    Takes in a histogram as an array and computes the sum of all (pixel intensity x number of pixels) values in
    the histogram

    :param numpy.ndarray, hist: histogram
    :return: total: the total sum
    """
    total = 0

    for pix_intensity, num_pix in enumerate(hist):
        total = total + ((pix_intensity + 1) * num_pix[0])

    return total


def draw_histograms_with_images(image,
                                gray=True,
                                show=True
                                ):
    """
    Display an image and its corresponding histogram(s).

    :param numpy.ndarray, image: image
    :param bool, gray: if image is a grayscale, then bool is True
    :param bool, show: if you want to view the image with histogram, then bool is True
    :return:
    """
    plt.figure()
    if gray is True:
        ax0 = plt.subplot2grid((1, 2), (0, 0))
        ax1 = plt.subplot2grid((1, 2), (0, 1))

        ax0.set_title('Gray scale image')
        ax0.imshow(image, cmap='gray_r')

        ax1.set_title('Gray scale histogram')
        hist = histogram(image)
        ax1.plot(hist,
                 color='k'
                 )
        ax1.set_xlabel('Pixel value')
        ax1.set_ylabel('Number of pixels')

        if show is True:
            plt.tight_layout()
            plt.show()

    else:
        ax0 = plt.subplot2grid((2, 2), (0, 0))
        ax1 = plt.subplot2grid((2, 2), (0, 1))
        ax2 = plt.subplot2grid((2, 2), (1, 0))
        ax3 = plt.subplot2grid((2, 2), (1, 1))

        ax0.set_title('Colour image')
        ax0.imshow(image)

        ax1.set_title('Blue histogram')
        hist1 = histogram(image, 0)  # blue channel
        ax1.plot(hist1, color='b')
        ax1.set_xlabel('Pixel value')
        ax1.set_ylabel('Number of pixels')

        ax2.set_title('Green histogram')
        hist2 = histogram(image, 1)  # green channel
        ax2.plot(hist2, color='g')
        ax2.set_xlabel('Pixel value')
        ax2.set_ylabel('Number of pixels')

        ax3.set_title('Red histogram')
        hist3 = histogram(image, 2)  # red channel
        ax3.plot(hist3, color='r')
        ax3.set_xlabel('Pixel value')
        ax3.set_ylabel('Number of pixels')

        if show is True:
            plt.tight_layout()
            plt.show()


def crop_horizontal(image, left, right):
    """
    Crops image in horizontal direction by the the crop_left fraction on the left and the crop_right fraction on
    the right.

    :param numpy.ndarray, image: image
    :param float, left: value between 0 and 1, is the fraction to crop out from the left part of the image
    :param float, right: value between 0 and 1, is the fraction to crop out from the right part of the image
    :return: numpy.ndarray, cropped, the image but with a fraction of the left and right parts of the image removed
    """
    height, width = height_width(image)
    new_left = int(width * left)
    crop_right = 1 - right
    new_right = int(width * crop_right)
    cropped = image[0:height, new_left:new_right]
    return cropped


def crop_vertical(image, top, bottom):
    """
    Crops image in horizontal direction by the the crop_left fraction on the left and the crop_right fraction on
    the right.

    :param numpy.ndarray, image: image
    :param float, top: value between 0 and 1, is the fraction to crop out from the top part of the image
    :param float, bottom: value between 0 and 1, is the fraction to crop out from the bottom part of the image
    :return: numpy.ndarray, cropped, the image but with a fraction of the top and bottom parts of the image removed
    """
    height, width = height_width(image)
    new_top = int(height * top)
    bottom = 1 - bottom
    new_bottom = int(height * bottom)
    cropped = image[new_top:new_bottom, 0:width]
    return cropped


class ImageAnalysis:
    def __init__(self):
        # things to do with selecting a rectangular area of an image
        self.frame_points = []  # list used for selecting a frame; inputs are lists: [x, y]
        self.list_of_frame_points_for_multiple_selection = []
        self.crop_left = None  # float, how much to crop from the left to get to the region of interest as a fraction
        #  of the entire image
        self.crop_right = None
        self.crop_top = None
        self.crop_bottom = None

        # things to do with selecting a polygonal area of an image, however, frame_points are still needed too to
        # know the points of the polygon
        self.mask_to_search_inside = None  # mask, inside of which to look for the liquid level - the region of
        # interest to search inside for the liquid level
        self.list_of_pixels_in_mask = None  # a list that contains lists. Each nested list is [row, col] that is the
        # location of an area within the mask the user selected
        self.frame_points_tuple = []  # list; same as self.frame_points except the inputs are tuples: (x, y),
        # used to help visualize region selection when selecting a polygonal area
        self.list_of_frame_points_tuple_for_multiple_selection = []
        self.list_of_masks_to_search_inside = []

    def reset(self):
        self.frame_points = []  # used for selecting a frame
        self.crop_left = None  # float, how much to crop from the left to get to the region of interest as a fraction
        #  of the entire image
        self.crop_right = None
        self.crop_top = None
        self.crop_bottom = None

    @staticmethod
    def display(image: np.ndarray,
                window_name: str = 'Image',
                ):
        """
        Display a cv2 image. User needs to press any key before anything else will happen. Image will stop being
        displayed when user exits out of the image window

        :param image:
        :return:
        """
        display(image, window_name)

    @staticmethod
    def display_image(image: np.ndarray,
                      image_name: str = 'Image',
                      ):
        warnings.warn(
            'use display() instead',
            DeprecationWarning,
            stacklevel=2,
        )
        ImageAnalysis.display(image=image,
                              window_name=image_name)

    def load_image(self,
                   image,
                   width: int = None,
                   height: int = None):
        """
        Load. If either width or height are not None, then the image will be resized using the imutils package so
        only width or height needs to be specified, and

        :param str, image, image: 'str' or numpy.ndarray: image to load
        :param int, width: image width you want the image to be resized to. height will be automatically adjusted to
            maintain image
        :param int, height: image height you want the image to be resized to. width will be automatically adjusted to
            maintain image
        :return: image: resized image as numpy.ndarray
        """
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        if type(image) is str:
            image = cv2.imread(image)

        if width is not None or height is not None:
            image = imutils.resize(image=image,
                                   width=width,
                                   height=height)

        return image

    @staticmethod
    def height_width(image,
                     ):
        """
        Helper method to find the height and width of an image, whether it is a grey scale image or not

        :param image: an image, as a numpy array
        :return: int, int: the height and width of an image
        """
        return height_width(image)

    def find_image_height_width(self, image):
        warnings.warn(
            'use width_height instead',
            DeprecationWarning,
            stacklevel=2,
        )
        image_height, image_width = self.height_width(image=image)
        return image_height, image_width

    def convert_to_grey(self,
                        image: np.ndarray,
                        ):
        """
        Convert an image to a grayscale version, thus eliminating hue and saturation in the image while
        retaining luminance.

        :param numpy.ndarray, image: a BGR image that has been loaded in by cv2
        :return: numpy.ndarray, fill: grayscale image
        """

        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        return image

    def get_histogram(self,
                      image,
                      channel=0,
                      ):
        """
        Get a histogram of the image; histogram of the number of pixels with a given intensity.

        :param numpy.ndarray, image: image
        :param int, channel: either 0 or 3, 0 if image is greyscale, 3 if it is RGB or BGR or hsv
        :return: numpy.ndarray, hist: 256 by 1 array, each value corresponds to the number of pixels in
            the image with its corresponding pixel value
        """
        return histogram(image, channel)

    def sum_histogram(self,
                      hist: np.ndarray,
                      ):
        """
        Takes in a histogram as an array and computes the sum of all (pixel intensity x number of pixels) values in
        the histogram

        :param numpy.ndarray, hist: histogram
        :return: total: the total sum
        """
        return sum_histogram(hist)

    def draw_histograms_with_images(self,
                                    image,
                                    gray=True,
                                    show=True
                                    ):
        """
        Display an image and its corresponding histogram(s).

        :param numpy.ndarray, image: image
        :param bool, gray: if image is a grayscale, then bool is True
        :param bool, show: if you want to view the image with histogram, then bool is True
        :return:
        """
        draw_histograms_with_images(image, gray, show)

    def select_rectangular_area(self, image, window_name='Select rectangular region'):
        """
        Allows user to see an image image, and to draw
        a box on the image and use that as the selected frame of where to look for something. After drawing a box
        you can press 'r' to clear it to reselect a different box or press 'c' to choose the box that will be
        the frame.

        :return: float, the fraction relative to the image size for each side that you would have to crop from to get
            the rectangular region of interest.
        """
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        # https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/
        # have it so that it opens a window so the user can click and drag to define the frame for the image, and then
        # that is the frame that should be set, so it also needs to set crop left, right, top, bottom

        image_height, image_width, _ = image.shape

        def make_selection(event, x, y, flags, param):
            # if left mouse button clicked, record the starting(x, y) coordinates
            if event is cv2.EVENT_LBUTTONDOWN:
                self.frame_points = [(x, y)]
            # check if left mouse button was released
            elif event is cv2.EVENT_LBUTTONUP:
                self.frame_points.append((x, y))

                # draw rectangle around selected region
                cv2.rectangle(image, self.frame_points[0], self.frame_points[1], (0, 255, 0), 2)
                cv2.imshow(window_name, image)

        # clone image and set up cv2 window
        image = image.copy()
        clone = image.copy()

        cv2.namedWindow(window_name)
        cv2.setMouseCallback(window_name, make_selection)

        # keep looping until 'q' is pressed
        while True:
            # display image, wait for a keypress
            cv2.imshow(window_name, image)
            key = cv2.waitKey(1) & 0xFF

            # if 'r' key is pressed, reset the cropping region
            if key == ord('r'):
                image = clone.copy()

            # if 'c' key pressed break from while True loop
            elif key == ord('c'):
                break

        # if there are 2 reference points then select the region of interest from the image to display
        if len(self.frame_points) == 2:
            cv2.namedWindow('Selected frame - roi')
            roi = clone[self.frame_points[0][1]:self.frame_points[1][1],
                  self.frame_points[0][0]:self.frame_points[1][0]]

            cv2.imshow('Selected frame - roi', roi)
            cv2.waitKey(0)

            # set the self.crop right, left, top, bottom from the selected frame
            self.crop_left = (self.frame_points[0][0] / image_width)
            self.crop_right = ((image_width - self.frame_points[1][0]) / image_width)
            self.crop_top = (self.frame_points[0][1] / image_height)
            self.crop_bottom = ((image_height - self.frame_points[1][1]) / image_height)

        cv2.destroyAllWindows()

        return self.crop_left, self.crop_right, self.crop_top, self.crop_bottom

    def find_rectangular_area_rows_and_columns(self, image, show=False, crop_left=None, crop_right=None, crop_top=None,
                                               crop_bottom=None):
        """
        Finds a frame to look for something within a closed image. Also finds it on the image, and has the option
        for user to view the image. What this returns are the row and column values for the region of interest that
        the user selected in self.select_rectangular_area; since select_rectangular_area returns a float to give the
        fraction you need to
        move inwards from the 4 edges of the image in order to get the frame, and so this function will give the row
        and column values that correspond to those float values for the image

        Example of using this method, which requires use of self.select_rectangular_area first to be run:
        crop_left, crop_right, top, bottom = self.select_rectangular_area(image)  # select rectangle on an image,
            gives the fraction to move inside from the edges of the image to get the selected frame (rectangle)
        left, right, top, bottom = self.find_rectangular_area_rows_and_columns(closed, crop_left=crop_left,
        crop_right=crop_right, top=top, bottom=bottom)  # gives row and column values for the
        image (absolute, not relative as is given by find_rectangular_area())

        Then you can do something like:
        row = self.find_something(image, left, right, top, bottom)

        Where the find_something() code iterates through the image according to an algorithm to find something
        or to do something, and the search can be constrained only to the  Example of this is find_liquid_level in
        LiquidLevel.

        Example of how left, right, top, and bottom can be used in find_something():
        def find_something():
            rows = range(top, bottom)
            cols = range(left, right)
            ...

        or the output of left, right, top, bottom can also be used to as parameters to crop an image to only get the
        selected rectangular area

        :param image: image
        :param float, crop_left: fraction, relative to image length, how much to go in from the left to get to the
            region on interest
        :param float, crop_right: fraction, relative to image length, how much to go in from the right to get to the
            region on interest
        :param float, crop_top: fraction, relative to image height, how much to go in from the top to get to the
            region on interest
        :param float, crop_bottom: fraction, relative to image height, how much to go in from the bottom to get to the
            region on interest
        :param bool, show: True to view the drawn frame on the closed image
        :return: left, right, top, bottom, are all int, that represent the row or column that together define the frame
            that is the region of interest
        """
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        if crop_left is None:
            crop_left = self.crop_left
        if crop_right is None:
            crop_right = self.crop_right
        if crop_top is None:
            crop_top = self.crop_top
        if crop_bottom is None:
            crop_bottom = self.crop_bottom

        if len(image.shape) is 3:
            image_height, image_width, channels = image.shape
        else:
            image_height, image_width = image.shape

        left = int(image_width * crop_left)
        right = image_width - int(image_width * crop_right)
        top = int(image_height * crop_top)
        bottom = image_height - int(image_height * crop_bottom)

        if show is True:
            image = image.copy()
            image = cv2.line(image, (left, top), (right, top), (0, 255, 0))
            image = cv2.line(image, (left, top), (left, bottom), (0, 255, 0))
            image = cv2.line(image, (left, bottom), (right, bottom), (0, 255, 0))
            image = cv2.line(image, (right, bottom), (right, top), (0, 255, 0))

            cv2.imshow('Selected frame', image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return left, right, top, bottom

    def select_multiple_polygonal_areas(self, image, number_of_areas_to_select: int, window_name='select multiple '
                                                                                                 'polygonal areas'):
        image_original = image
        image_clone = image.copy()  # cloned image to have all selected drawn lines show when selecting more than
        # one polygonal area
        print(f'select multiple polygonal area. press "c" to choose an area, press "r" to restart a single selection')

        self.list_of_frame_points_for_multiple_selection = []
        self.list_of_frame_points_tuple_for_multiple_selection = []
        self.list_of_masks_to_search_inside = []

        for i in range(number_of_areas_to_select):
            self.select_polygonal_area(image=image_clone, window_name=window_name)
            selected_frame_points = self.frame_points
            selected_frame_points_tuple = self.frame_points_tuple
            self.list_of_frame_points_for_multiple_selection.append(selected_frame_points)
            self.list_of_frame_points_tuple_for_multiple_selection.append(selected_frame_points_tuple)
            self.list_of_masks_to_search_inside.append(self.mask_to_search_inside)

        image_with_drawn_areas = self.draw_multiple_selected_polygonal_areas_on_image(image=image)
        masked_image = self.draw_mask_on_image(image=image)

        cv2.imshow("masked image", masked_image)
        cv2.imshow("image with selected area", image_with_drawn_areas)

        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def select_polygonal_area(self, image, window_name='Select polygonal region'):
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        # todo right now this is copy and paste from liquid_level.py with slight tweaks to variable names
        """
        Allows user to see an image of both the latest loaded image and the contour version of that image, and to draw
        a polygon on the image and use that as the selected frame of where to look for a meniscus. After drawing,
        you can press 'r' to clear it to reselect a different box or press 'c' to choose the box that will be
        the frame.


        This sets self.mask_to_search_inside, list_of_frame_points_frame_points_tuple,
        and list_of_frame_points_frame_points_list in order to create the mask for the region of interest

        :param image: image that can be displayed by opencv
        :param str, window_name: name for the opencv window displayed for the user to select regions
        """
        # https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/
        # have it so that it opens a window so the user can click and drag to define the frame for the image, and then
        # that is the frame that should be set, so it also needs to  set crop left, right, top, bottom

        # todo make a change so  that will resize the image to the size of the screen, which means need to track the
        # old and new image width and height in order to convert between the two since getting the x and y points in
        # an image of the resized image will not be the same as for the original image size
        # when making points to connect, the last point will be drawn as connected to the first point in the mask
        # when trying to draw it

        # reset any selections made before
        self.mask_to_search_inside = None
        self.frame_points = []
        self.frame_points_tuple = []
        # also need to add stuff to make a mask

        def make_selection(event, x, y, flags, param):
            # if left mouse button clicked, record the starting(x, y) coordinates
            if event is cv2.EVENT_LBUTTONDOWN:
                self.frame_points_tuple.append((x, y))
                self.frame_points.append([x, y])

            if event is cv2.EVENT_LBUTTONUP:
                if len(self.frame_points_tuple) >= 2:
                    # if there are a total of two or more clicks made on the image, draw a line to connect the dots
                    # to easily visualize the region of interest that has been created so far
                    cv2.line(image, self.frame_points_tuple[-2],
                             self.frame_points_tuple[-1], (0, 255, 0), 2)

            if len(self.frame_points_tuple) > 0:
                # I think this means that even if only a single click has occurred so far, to still draw a single line
                #  that starts and ends at the position (aka a dot) to allow visualization of where the user just
                # clicked
                cv2.line(image, self.frame_points_tuple[-1],
                         self.frame_points_tuple[-1],
                         (0, 255, 0), 2)
                cv2.imshow(window_name, image)

        # clone image and set up cv2 window
        image = image.copy()
        clone = image.copy()

        cv2.namedWindow(window_name)
        cv2.setMouseCallback(window_name, make_selection)

        # keep looping until 'q' is pressed - q to quit
        while True:
            # during this time the user can left click on the image to select the region of interest

            # display image, wait for a keypress
            cv2.imshow(window_name, image)
            key = cv2.waitKey(1) & 0xFF

            # if 'r' key is pressed, reset the cropping region
            if key == ord('r'):
                image = clone.copy()
                self.frame_points = []
                self.frame_points_tuple = []

            # if 'c' key pressed break from while True loop
            elif key == ord('c'):
                # make into np array because this is the format it is needed to make a mask
                self.frame_points = np.array(self.frame_points)
                break

        # create mask inside of which the liquid level will be searched for
        # use this for help:
        # https://stackoverflow.com/questions/48301186/cropping-concave-polygon-from-image-using-opencv-python

        self.mask_to_search_inside = self.image_mask(image=clone, frame_points=self.frame_points)

        # do bit wise operation, this gives the original image back but with only selected region showing,
        # and everything else is black (aka apply a mask, where only the things inside the mask,
        # which is self.mask_to_search_inside, will be visible and the rest of the image blacked out). This is just
        # to show that creating the self.mask_to_search_inside worked.
        clone = self.draw_selected_polygonal_area_on_image(image=clone)
        masked_image = cv2.bitwise_and(clone, clone, mask=self.mask_to_search_inside)
        cv2.imshow("image with outlined area", clone)
        cv2.imshow("masked image", masked_image)

        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def image_mask(self, image, frame_points):
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        """ create a mask (black background, but white within the frame points)"""
        # create initial blank (all black) mask. For the mask, when it is applied to an image, pixels that coincide
        # with black pixels on the mask will not be shown
        mask_to_search_inside = np.zeros(shape=image.shape[0:2], dtype=np.uint8)

        # make the mask; connect the points that the user selected to create the mask area, and make that area white
        # pixels. the mask automatically connects the first and last points that were made, to create an enclosed
        # area for the mask
        cv2.drawContours(mask_to_search_inside, [frame_points], -1,
                         (255, 255, 255), -1, cv2.LINE_AA)
        return mask_to_search_inside

    def draw_multiple_selected_polygonal_areas_on_image(self, image):
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        for i in range(len(self.list_of_frame_points_for_multiple_selection)):
            image = self.draw_selected_polygonal_area_on_image(image=image,
                                                               list_of_polygon_points=self.list_of_frame_points_for_multiple_selection[i])

        return image

    def draw_selected_polygonal_area_on_image(self, image, list_of_polygon_points=None):
        """
        Display the one selected polygonal area on an image. This is for showing a single selected polygonal area

        :param list_of_polygon_points: list of points for a single polygonal area; so for a single selected polygonal
            area this would be self.frame_points, which is also the default value if nothing is passed in
        :return:
        """
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        # draw mask outline on the  image, then return the image
        if image.shape is 2:  # if image is black and white, not rgb
            line_colour = (255, 255, 255)  # make line colour white
        else:
            # cv image is rgb
            line_colour = (0, 255, 0)  # make line colour green

        if list_of_polygon_points is None:
            list_of_polygon_points = [self.frame_points]
        else:
            list_of_polygon_points = [list_of_polygon_points]

        image = cv2.drawContours(image,
                                 list_of_polygon_points,
                                 -1,
                                 line_colour,
                                 1,
                                 cv2.LINE_AA)
        return image

    def put_text_near_polygonal_area(self,
                                     image,
                                     text: str,
                                     list_of_polygon_points=None,
                                     font=cv2.FONT_HERSHEY_SIMPLEX,
                                     font_scale=1,
                                     colour=(0, 255, 0),
                                     ):
        """
        Write some text near/to the right of a polygonal area
        bgr_green = (0, 255, 0)
        """
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        left, right, top, _ = self.find_maximum_edges_of_mask(list_of_polygon_points)
        middle = int((left + right) / 2)
        text_position = (middle, top - 20)
        cv2.putText(image, text, text_position, font, font_scale, colour)
        return image

    def draw_mask_on_image(self, image):
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        # draw mask on the  image, then return the image
        line_colour = (255, 255, 255)  # make line colour white

        # draw outline of shape on the image to better see edges of the mask
        if self.list_of_frame_points_for_multiple_selection == []:
            image = self.draw_selected_polygonal_area_on_image(image=image)
            # make the mask; connect the points that the user selected to create the mask area, and make that area white
            # pixels. the mask automatically connects the first and last points that were made, to create an enclosed
            # area for the mask
            cv2.drawContours(self.mask_to_search_inside,
                             [self.frame_points],
                             -1,
                             line_colour,
                             -1,
                             cv2.LINE_AA)
            # put mask on image, and black out anything not in the area you want to search in
            # do bit wise operation, this gives the original image back but with only selected region showing,
            # and everything else is black (aka apply a mask, where only the things inside the mask,
            # which is self.mask_to_search_inside, will be visible and the rest of the image blacked out). This is just
            # to show that creating the self.mask_to_search_inside worked.
            image = cv2.bitwise_and(image, image, mask=self.mask_to_search_inside)

        else:
            image = self.draw_multiple_selected_polygonal_areas_on_image(image=image)
            all_masks = np.zeros(shape=image.shape[0:2], dtype=np.uint8)
            for i in range(len(self.list_of_masks_to_search_inside)):
                all_masks = all_masks + self.list_of_masks_to_search_inside[i]

            # if when putting all the masks together there is overlap, change the value in the mask to 1
            image_height, image_width = self.find_image_height_width(image)
            for row in range(image_height):
                for column in range(image_width):
                    if all_masks[row][column] > 1:
                        all_masks[row][column] = 1

            self.mask_to_search_inside = []
            self.frame_points = []
            self.frame_points_tuple = []

            # do bit wise operation, this gives the original image back but with only selected region showing,
            # and everything else is black (aka apply a mask, where only the things inside the mask,
            # which is self.mask_to_search_inside, will be visible and the rest of the image blacked out). This is just
            # to show that creating the self.mask_to_search_inside worked.
            image = cv2.bitwise_and(image, image, mask=all_masks)

        return image

    def find_maximum_edges_of_mask(self, list_of_polygon_points=None):
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        """
        :return: left, right, top, bottom, are all int, that represent the row or column that together define
        the edges of the mask. So left would be the left column, and top would be the top row, and so on
        """
        # from the mask to search inside of, find the row and column values that together would make a
        # rectangle surrounding the area that encompasses all non zero values of the mask. top, would be the top row,
        # right would be the right column, and so on
        left = 1000*1000
        right = 0
        top = 1000*1000
        bottom = 0
        if list_of_polygon_points is None:
            list_of_polygon_points = self.frame_points
        for idx, pixel_point_value in enumerate(list_of_polygon_points):
            # loop through all the points that the user selected to create the outline of the mask inner list is
            # a list of the x and y coordinates of that point
            if pixel_point_value[0] < left:
                left = pixel_point_value[0]
            if pixel_point_value[0] > right:
                right = pixel_point_value[0]
            if pixel_point_value[1] < top:
                top = pixel_point_value[1]
            if pixel_point_value[1] > bottom:
                bottom = pixel_point_value[1]
        return left, right, top, bottom

    def get_list_of_pixels_in_mask_to_search_for(self) -> List[np.ndarray]:
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        list_of_pixels_in_mask = []  # is a list, and inside will be lists, where each list is the
        # [row, column] for a pixel that is within the mask/is a pixel that was contained in the user selected
        # polygonal area

        # get the edges of the mask so you can cut down on what pixels you need to check in order to get the pixels
        # within the mask

        left_col, right_col, top_row, bottom_row = self.find_maximum_edges_of_mask()

        rows = range(top_row, bottom_row)  # rows
        cols = range(left_col, right_col)  # columns

        for row in rows:
            for col in cols:
                if self.mask_to_search_inside[row][col] > 0:
                    # if in the mask the value is greater than 0 at that pixel, it means that the pixels is a part of
                    # the selection area/mask to search inside of
                    list_of_pixels_in_mask.append([row, col])

        self.list_of_pixels_in_mask = list_of_pixels_in_mask

        return list_of_pixels_in_mask

    def get_image_pixels_only_in_polygonal_roi(self, image) -> List[np.ndarray]:
        warnings.warn(
            'will be removed in the future, do not use',
            FutureWarning,
            stacklevel=2,
        )
        """
        return a list of the pixel values within the mask that the user chose from during the polygonal area
        selection function

        :param image: an image that has already been loaded; numpy array, can't be a string
        :return:
        """
        list_of_image_pixels_within_mask = []  # a list of the pixels of an image that fall within the mask to search
        #  inside of/the polygonal roi

        if self.list_of_pixels_in_mask is None:
            self.get_list_of_pixels_in_mask_to_search_for()

        for pixel_location in self.list_of_pixels_in_mask:
            row: int = pixel_location[0]
            col: int = pixel_location[1]
            image_pixel = image[row][col]
            list_of_image_pixels_within_mask.append(image_pixel)

        return list_of_image_pixels_within_mask


class VideoStepper:
    def __init__(self,
                 target_path: str,
                 start_frame: int = None,
                 end_frame: int = None,
                 ):
        """
        A class for stepping through a video for frame-by-frame analyses

        :param target_path: target video path
        :param start_frame: start frame for iteration
        :param end_frame: end frame for iteration
        """
        if type(target_path) is not str:
            target_path = str(target_path)
        # create capture and wait for it to open
        self.cap = cv2.VideoCapture(target_path)
        while not self.cap.isOpened():
            self.cap = cv2.VideoCapture(target_path)
            cv2.waitKey(50)

        # set start and end frames
        if start_frame is None:
            start_frame = 1
        self.start_frame = start_frame
        if end_frame is None:
            end_frame = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self.end_frame = end_frame

    def __iter__(self):
        while self.cap.isOpened():
            # step up to start frame
            if self.current_frame < self.start_frame:
                self.current_frame = self.start_frame
            # retrieve next frame
            flag, frame = self.cap.read()
            if flag is False:
                self.end_frame = int(
                    self.cap.get(cv2.CAP_PROP_POS_FRAMES)
                )
                break  # todo is there a case where this isn't supposed to happen?
            # if not flag:  # if it wasn't ready, reset and retry
            #     self.cap.set(
            #         cv2.CAP_PROP_POS_FRAMES,
            #         cv2.CAP_PROP_POS_FRAMES - 1,
            #     )
            #     cv2.waitKey(50)
            #     continue
            # if outside of frame range, continue or break out accordingly
            if self.cap.get(cv2.CAP_PROP_POS_FRAMES) > self.end_frame:
                break
            yield frame

    def __len__(self):
        return self.end_frame - self.start_frame

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cap.release()

    @property
    def fps(self) -> float:
        """frames per second"""
        return self.cap.get(
            cv2.CAP_PROP_FPS
        )

    @property
    def frame_duration(self) -> float:
        return 1 / self.fps

    def frame_from_time(self, frame_time: float):
        """
        Calculates the frame number for the specified time

        :param frame_time: time in seconds
        :return: estimated frame number
        """
        return round(frame_time * self.fps)

    @property
    def current_frame(self) -> int:
        """current frame in the video"""
        return int(self.cap.get(cv2.CAP_PROP_POS_FRAMES))

    @current_frame.setter
    def current_frame(self, value: Union[float, int]):
        self.cap.set(
            cv2.CAP_PROP_POS_FRAMES,
            value
        )

    def get_time_of_frame(self, frame: Union[float, int]):
        """
        Gets the time in ms of the specified frame

        :param frame: frame to retrieve from
        :return: time in ms
        """
        # get current frame
        curr_frame = self.cap.get(cv2.CAP_PROP_POS_FRAMES)
        self.cap.set(  # change frame to start frame
            cv2.CAP_PROP_POS_FRAMES,
            frame,
        )
        out = self.cap.get(cv2.CAP_PROP_POS_MSEC)  # get time (ms)
        self.cap.set(  # reset frame to current frame
            cv2.CAP_PROP_POS_FRAMES,
            curr_frame,
        )
        return out

    @property
    def start_frame_time(self) -> float:
        """time (ms) for start frame"""
        return self.get_time_of_frame(self.start_frame)

    @property
    def end_frame_time(self) -> float:
        """time (ms) for end frame"""
        return self.get_time_of_frame(self.end_frame)


