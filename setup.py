import os
import setuptools
from setuptools import find_packages, setup

NAME = 'heinsight_liquid_level_H2'
DESCRIPTION = """HeinSight is a computer-vision based control system for automating common laboratory tasks. The
Liquid-Level Hydrogen-2 release is a prototype designed to facilitate the remote monitoring and control of a liquid-air
 interface in a transparent vessel. Sample workflows have been provided for the following experimental types that
 require continuous stirring: dual pump continuous preferential crystallization (CPC), continuous
 distillation, and filtration"""
AUTHOR = 'Veronica Lai, Tara Zepel, Lars Yunker // Hein Group'
CLASSIFIERS = [
    'Programming Language :: Python :: 3',
    'Operating System :: Windows',
]
INSTALL_REQUIRES = [
    'opencv-python',
    'numpy',
    'pandas',
    'slackclient==2.0.1',
    'pyserial',
    'imutils',
    'matplotlib'
]

with open('LICENSE') as f:
    lic = f.read()
    lic.replace('\n', ' ')

PACKAGES = find_packages()

setup(
    name=NAME,
    version='1.0',
    description=DESCRIPTION,
    author=AUTHOR,
    url='https://gitlab.com/heingroup/heinsight_liquid_level_H2',
    install_requires=INSTALL_REQUIRES,
    packages=PACKAGES,
    license=lic,
    classifiers=CLASSIFIERS,
)
