"""
Maintain liquid level above a lower limit using a new era peristaltic pump. Record images and videos of what the
camera sees (with and without annotations drawn on the image), a csv file of the liquid level in the images used
for level analysis, and a json file of the roi selection and additional parameters for for the liquid level monitor
object. Additionally, a csv of the total number of dispenses made over time is also created

If after a time out number of minutes has passed without any dispenses being made, a Slack message will be sent
informing you of this

Script must be run in debug mode. This is because to end the script, you need to set a breakpoint at the while run
line, then when the script is paused, set run = False, then resume the script, and soon after the run will stop. Or,
the script can be ended through Slack by sending the 'end run' command

For slack integration, there needs to be a file called slack_manager_info.py in the same folder as this script. In it
there needs to be two variables, one called 'bot_token', which is the slack bot token, and one called 'channel_name',
which is the name of the Slack channel to send updates and receive commands to (note that the name of the channel
must be prefixed by  #)

Possible Slack commands that can be sent are 'slack image', 'dispense', and 'end run'

This script has been used for automated filtration and solvent swap
"""
import time
from pathlib import Path
import logging
import re
from hein_utilities.runnable import Runnable
from new_era.peristaltic_pump import PeristalticPump
from heinsight.vision.liquid_level import LiquidLevelMonitorRunnable, LiquidLevelMonitor
from heinsight.vision_utilities.camera import Camera
from heinsight.vision_utilities.roi import ROI
from datetime import datetime
from heinsight.heinsight_utilities.temporal_data import TemporalData
from hein_utilities.slack_integration.parsing import ignore_bot_users
from hein_utilities.slack_integration.bots import WebClientOverride
from hein_utilities.slack_integration.slack_managers import RTMControlManager

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


if __name__ == '__main__':
    # todo adjust pump time and rate as needed to ensure that self correction will maintain the level above the lower
    #  limit
    pump = PeristalticPump('COM12')  # todo ensure correct port
    pump_time = 1  # seconds
    rate = 5  # ml/min
    pump_direction = 'dispense'

    # set up file saving
    root = Path.cwd()  # todo change this to change the root directory if needed
    i = 1

    output_name = 'filtration'  # todo edit to change the name of output files
    output_folder = output_name
    data = root.joinpath(output_folder)  # todo edit to change folder to save all data in; folder must be in the root folder
    while data.exists():
        data = root.joinpath(f'{output_folder} - {i}')
        i += 1
    data.mkdir()
    output = data.joinpath(output_name)
    stream = data.joinpath('stream.mp4')  # todo change to edit the name of the camera stream video

    # for saving time course data on when additions occur
    time_course_addition_csv_path = data.joinpath('addition')
    time_course_addition_data = TemporalData()
    time_course_addition_data.save_path = time_course_addition_csv_path

    port = 0
    # todo change to 0 if only 1 webcam and computer has no webcam, change to 1 if computer has a webcam and you
    #  want to use an external webcam
    camera = Camera(port, save_folder=data.joinpath('images'), datetime_string_format='%Y_%m_%d_%H_%M_%S')
    llm = LiquidLevelMonitor(output)
    limit = 0.6

    try:
        from slack_manager_info import bot_token, channel_name
        slack_manager = RTMControlManager(token=bot_token, channel_name=channel_name)
        from slack_manager_info import bot_token, channel_name
        logger.addHandler(slack_manager)
    except Exception as e:
        slack_manager = None

    # set roi and lower limit reference line
    frame = camera.frame()
    roi_name = 'roi'
    llm.add_roi(frame, ROI.polygon_roi, roi_name)
    llm.add_reference(frame, roi_name, False, True)

    # create runnable
    n_images = 20  # number of images to use to mae a single liquid level measurements
    llm = LiquidLevelMonitorRunnable(camera, llm, output, n_images)
    llm.monitor.save_json()
    llm.camera_video_path = str(stream.absolute())
    llm.save_photos = True

    # use the stream function to view what the camera sees. press the 'q' button to exit out of the stream
    llm.stream()

    # todo should only add washing for filtration not solvent swap
    # time out that automatically triggers dispensing to wash residue off funnel
    timeout_mins = 20

    def dispense():
        pump.pump(pump_time=pump_time,
                  direction=pump_direction,
                  rate=rate,
                  )


    class MaintainLevel(Runnable):
        def __init__(self,
                     llm: LiquidLevelMonitorRunnable,
                     n: int = 5
                     ):
            Runnable.__init__(self)
            self.llm = llm
            self.n = n  # number of measurements before making a decision to adjust level or not
            self.pause = False
            self.addition_index = 0
            self.last_addition_time = datetime.now()

        def run(self):
            while self.running:
                if self.pause is True:
                    time.sleep(10)
                    continue

                self.llm.monitor.save_csv()
                time_course_addition_data.save_csv()
                if f'{roi_name} good' in self.llm.monitor.columns:
                    good = self.llm.monitor.tail(self.n, f'{roi_name} good')
                    if len(good) == self.n:
                        good = good.mean()
                        print(f'good: {good}')
                        # if at least limit % of measurements have a level below the lower limit then need to dispense
                        if good >= limit:
                            pass
                        else:
                            level = self.llm.monitor.tail(self.n, roi_name)
                            level = level.median()
                            # if level is below the lower limit
                            if level <= self.llm.monitor.reference_manager.reference(roi_name).lower:
                                print('level is below, dispense')
                                t = datetime.now()
                                self.last_addition_time = t
                                self.addition_index += 1
                                add_data = {'addition': self.addition_index}
                                time_course_addition_data.add_data(add_data, t=t)
                                dispense()
                            else:
                                print('this should never be printed')

        def start(self):
            self.llm.start_monitoring()
            time.sleep(15)
            super().start()

        def stop(self) -> None:
            super().stop()
            self.llm.camera.stop_recording()
            self.llm.stop_monitoring()
            pump.stop()


    maintain = MaintainLevel(llm)
    maintain.start()
    time.sleep(1)
    # get the start time from the data from the liquid level monitor, and set that as the start time for the addition
    # csv file to have the 0 point
    start_time: str = maintain.llm.monitor.data.head(1)[maintain.llm.monitor.time_heading][0]
    add_data = {
        'addition': 0,
    }
    time_course_addition_data.add_data(add_data, t=start_time)

    def pause():
        maintain.pause = True

    def resume():
        maintain.pause = False

    # todo to end, set a breakpoint at the while line below, then when the script is paused used the python console to
    #  set run = False, then continue and this will soon end the run
    run = True

    # set up slack integration commands if slack manager is instantiated - uou can ignore this block
    if slack_manager is not None:
        import threading
        try:
            @slack_manager.run_on(event='message')
            @ignore_bot_users
            def catch_message(**payload):
                message = payload['data']
                text = str(message.get('text'))
                web_client = payload['web_client']
                try:
                    if re.fullmatch('slack image', text, re.IGNORECASE) is not None:
                        with WebClientOverride(slack_manager, web_client):
                            try:
                                last_image_path = camera.save_folder.parent.joinpath('last image.png')
                                camera.take_photo(name=last_image_path)
                                slack_manager.post_slack_file(filepath=str(last_image_path), title='Last image taken', comment='Last image taken')
                            except Exception as e:
                                logger.error(f'error encountered trying to send photo from Slack : {e}')
                    elif re.fullmatch('dispense', text, re.IGNORECASE) is not None:
                        with WebClientOverride(slack_manager, web_client):
                            try:
                                t = datetime.now()
                                maintain.last_addition_time = t
                                maintain.addition_index += 1
                                add_data = {'addition': maintain.addition_index}
                                time_course_addition_data.add_data(add_data, t=t)
                                dispense()
                            except Exception as e:
                                logger.error(f'error encountered trying to dispense from Slack : {e}')
                    elif re.fullmatch('end run', text, re.IGNORECASE) is not None:
                        with WebClientOverride(slack_manager, web_client):
                            try:
                                global run
                                run = False
                                logger.info('ending the run now, follow up message will be sent when run has '
                                            'successfully ended')
                            except Exception as e:
                                logger.error(f'error encountered trying to end the run from Slack : {e}')
                    elif re.fullmatch('help', text, re.IGNORECASE) is not None:
                        with WebClientOverride(slack_manager, web_client):
                            try:
                                help_statement = f'Possible commands:\n' \
                                                 f'*slack image* - send the current camera view\n' \
                                                 f'*dispense* - trigger a dispense\n' \
                                                 f'*end run* - end the run'
                                logger.info(help_statement)
                            except Exception as e:
                                logger.error(f'error encountered trying to send help response to Slack : {e}')
                except Exception as e:
                    logger.error(f'error encountered: {e}')
            threading.Thread(target=slack_manager.start_rtm_client).start()
        except Exception as e:
            pass

    while run:
        if (datetime.now() - maintain.last_addition_time).seconds >= timeout_mins * 60:
            logger.info(f'system has been idle for {timeout_mins}. if it looks like everything has filtered. '
                        f'end the run by sending the "end run" command')
            t = datetime.now()
            maintain.last_addition_time = t
        time.sleep(5)

    try:
        maintain.stop()
    except Exception as e:
        pass

    t = datetime.now()
    add_data = {'addition': maintain.addition_index}
    time_course_addition_data.add_data(add_data, t=t)

    time_course_addition_data.save_csv()
    logger.info('finished successfully')







