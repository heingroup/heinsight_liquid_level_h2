"""
Maintain liquid level above a lower limit using a new era peristaltic pump. Record images and videos of what the
camera sees (with and without annotations drawn on the image), a csv file of the liquid level in the images used
for level analysis, and a json file of the roi selection and additional parameters for for the liquid level monitor
object. Additionally, a csv of the total number of dispenses made over time is also created

Script must be run in debug mode. This is because to end the script, you need to set a breakpoint at the while run
line, then when the script is paused, set run = False, then resume the script, and soon after the run will stop

This script has been used for automated filtration and solvent swap
"""
import time
from pathlib import Path
from hein_utilities.runnable import Runnable
from new_era.peristaltic_pump import PeristalticPump
from heinsight.vision.liquid_level import LiquidLevelMonitorRunnable, LiquidLevelMonitor
from heinsight.vision_utilities.camera import Camera
from heinsight.vision_utilities.roi import ROI
from datetime import datetime
from heinsight.heinsight_utilities.temporal_data import TemporalData


if __name__ == '__main__':
    # todo adjust pump time and rate as needed to ensure that self correction will maintain the level above the lower
    #  limit
    pump = PeristalticPump('COM12')  # todo ensure correct port
    pump_time = 1  # seconds
    rate = 0.5  # ml/min
    pump_direction = 'dispense'

    # set up file saving
    root = Path.cwd()  # todo change this to change the root directory if needed
    i = 1

    output_name = 'solvent swap'  # todo edit to change the name of output files
    output_folder = output_name
    data = root.joinpath(output_folder)  # todo edit to change folder to save all data in; folder must be in the root folder
    while data.exists():
        data = root.joinpath(f'{output_folder} - {i}')
        i += 1
    data.mkdir()
    output = data.joinpath(output_name)
    stream = data.joinpath('stream.mp4')  # todo change to edit the name of the camera stream video

    # for saving time course data on when additions occur
    time_course_addition_csv_path = data.joinpath('addition')
    time_course_addition_data = TemporalData()
    time_course_addition_data.save_path = time_course_addition_csv_path

    port = 0
    # todo change to 0 if only 1 webcam and computer has no webcam, change to 1 if computer has a webcam and you
    #  want to use an external webcam
    camera = Camera(port, save_folder=data.joinpath('images'), datetime_string_format='%Y_%m_%d_%H_%M_%S')
    llm = LiquidLevelMonitor(output)
    limit = 0.6

    # set roi and lower limit reference line
    frame = camera.frame()
    roi_name = 'roi'
    llm.add_roi(frame, ROI.polygon_roi, roi_name)
    llm.add_reference(frame, roi_name, False, True)

    # create runnable
    n_images = 20  # number of images to use to mae a single liquid level measurements
    llm = LiquidLevelMonitorRunnable(camera, llm, output, n_images)
    llm.monitor.save_json()
    llm.camera_video_path = str(stream.absolute())
    llm.save_photos = True

    # use the stream function to view what the camera sees. press the 'q' button to exit out of the stream
    llm.stream()

    def dispense():
        pump.pump(pump_time=pump_time,
                  direction=pump_direction,
                  rate=rate,
                  )


    class MaintainLevel(Runnable):
        def __init__(self,
                     llm: LiquidLevelMonitorRunnable,
                     n: int = 15
                     ):
            Runnable.__init__(self)
            self.llm = llm
            self.n = n  # number of measurements before making a decision to adjust level or not

        def run(self):
            addition_index = 0
            while self.running:
                self.llm.monitor.save_csv()
                time_course_addition_data.save_csv()
                if f'{roi_name} good' in self.llm.monitor.columns:
                    good = self.llm.monitor.tail(self.n, f'{roi_name} good')
                    if len(good) == self.n:
                        good = good.mean()
                        print(f'good: {good}')
                        if good >= limit:
                            pass
                        # if at least limit % of measurements have a level below the lower limit then need to dispense
                        else:
                            level = self.llm.monitor.tail(self.n, roi_name)
                            level = level.median()
                            # if level is below the lower limit
                            if level <= self.llm.monitor.reference_manager.reference(roi_name).lower:
                                print('level is below, dispense')
                                t = datetime.now()
                                addition_index += 1
                                add_data = {'addition': addition_index}
                                time_course_addition_data.add_data(add_data, t=t)
                                dispense()

        def start(self):
            self.llm.start_monitoring()
            time.sleep(15)
            super().start()

        def stop(self) -> None:
            super().stop()
            pump.stop()
            self.llm.stop_monitoring()


    maintain = MaintainLevel(llm)
    maintain.start()
    time.sleep(1)
    start_time: str = maintain.llm.monitor.data.head(1)[maintain.llm.monitor.time_heading][0]
    add_data = {
        'addition': 0,
    }
    time_course_addition_data.add_data(add_data, t=start_time)

    # todo to end, set a breakpoint at the while line, then when the script is paused used the python console to
    #  set run = False, then continue and this will soon end the run
    run = True
    while run:
        time.sleep(5)

    maintain.stop()
    time_course_addition_data.save_csv()

    print('done')
