"""
Controlled bidirectional liquid cycling using 2 new era peristaltic pumps with 2 vessels.
Maintain liquid level one and thus both vessels between a lower and upper limit using two new era peristaltic pump.

Record images and videos of what the camera sees (with and without annotations drawn on the image), a csv file of
the liquid level in the images used for level analysis, and a json file of the roi selection and additional
parameters for for the liquid level monitor object

Both pumps need to be set up so that the dispense direction of both pumps goes into the vessel that has the webcam
aimed at it

Script must be run in debug mode. This is because to end the script, you need to set a breakpoint at the while run
line, then when the script is paused, set run = False, then resume the script, and soon after the run will stop

This script has been used for automated continuous bidirectional liquid transfer between two vessels with two pumps
"""
import time
from pathlib import Path
from hein_utilities.runnable import Runnable
from new_era.peristaltic_pump import PeristalticPump
from heinsight.vision.liquid_level import LiquidLevelMonitorRunnable, LiquidLevelMonitor
from heinsight.vision_utilities.camera import Camera
from heinsight.vision_utilities.roi import ROI
from datetime import datetime
from heinsight.heinsight_utilities.temporal_data import TemporalData


if __name__ == '__main__':
    # todo adjust pump time and rate as needed to ensure that self correction will maintain the level above the lower
    #  limit
    dispense_pump = PeristalticPump('COM12')  # todo ensure correct port
    dispense_pump_time = 1  # seconds
    dispense_rate = 1  # ml/min

    withdraw_pump = PeristalticPump('COM14')  # todo ensure correct port
    withdraw_pump_time = 1  # seconds
    withdraw_rate = 1  # ml/min

    dispense_pump.set_rate(dispense_rate)
    dispense_pump.set_direction('dispense')
    withdraw_pump.set_rate(withdraw_rate)
    withdraw_pump.set_direction('withdraw')

    # set up file saving
    root = Path.cwd()  # todo change this to change the root directory if needed
    i = 1

    output_name = 'cpc'  # todo edit to change the name of output files
    output_folder = output_name
    data = root.joinpath(output_folder)  # todo edit to change folder to save all data in; folder must be in the root folder
    while data.exists():
        data = root.joinpath(f'{output_folder} - {i}')
        i += 1
    data.mkdir()
    output = data.joinpath(output_name)
    stream = data.joinpath('stream.mp4')  # todo change to edit the name of the camera stream video

    # for saving time course data on when additions occur
    time_course_addition_csv_path = data.joinpath('addition')
    time_course_addition_data = TemporalData()
    time_course_addition_data.save_path = time_course_addition_csv_path

    port = 0
    # todo change to 0 if only 1 webcam and computer has no webcam, change to 1 if computer has a webcam and you
    #  want to use an external webcam
    camera = Camera(port, save_folder=data.joinpath('images'), datetime_string_format='%Y_%m_%d_%H_%M_%S')
    llm = LiquidLevelMonitor(output)
    limit = 0.6

    # set roi and upper and lower limit reference lines
    frame = camera.frame()
    roi_name = 'roi'
    llm.add_roi(frame, ROI.polygon_roi, roi_name)
    llm.add_reference(frame, roi_name, True, True)

    # create runnable
    n_images = 15  # number of images to use to make a single liquid level measurements
    llm = LiquidLevelMonitorRunnable(camera, llm, output, n_images)
    llm.monitor.save_json()
    llm.camera_video_path = str(stream.absolute())
    llm.save_photos = True

    # use the stream function to view what the camera sees. press the 'q' button to exit out of the stream
    llm.stream()

    def start_pumps():
        dispense_pump.start()
        withdraw_pump.start()

    def stop_pumps():
        dispense_pump.stop()
        withdraw_pump.stop()

    def dispense():
        dispense_pump.pump(pump_time=dispense_pump_time,
                           direction='dispense',
                           rate=dispense_rate,
                           )

    def withdraw():
        withdraw_pump.pump(pump_time=withdraw_pump_time,
                           direction='withdraw',
                           rate=withdraw_rate,
                           )

    class MaintainLevel(Runnable):
        def __init__(self,
                     llm: LiquidLevelMonitorRunnable,
                     n: int = 15
                     ):
            Runnable.__init__(self)
            self.llm = llm
            self.n = n  # number of measurements before making a decision to adjust level or not
            self.pause = False
            self.dispense_index = 0
            self.withdraw_index = 0

        def run(self):
            while self.running:
                if self.pause is True:
                    time.sleep(15)
                    continue
                self.llm.monitor.save_csv()
                time_course_addition_data.save_csv()
                if f'{roi_name} good' in self.llm.monitor.columns:
                    good = self.llm.monitor.tail(self.n, f'{roi_name} good')
                    if len(good) == self.n:
                        good = good.mean()
                        print(f'good: {good}')
                        # if at least limit % of measurements have a level below the lower limit then need to
                        # dispense, or if limit % of measurements have a level above the upper limit then need to
                        # withdraw
                        if good >= limit:
                            pass
                        else:
                            level = self.llm.monitor.tail(self.n, roi_name)
                            level = level.median()
                            # if level is below the lower limit
                            if level <= self.llm.monitor.reference_manager.reference(roi_name).lower:
                                stop_pumps()
                                print('level is below, dispense')
                                self.dispense_index += 1
                                dispense()
                                add_data = {
                                    'dispense': self.dispense_index,
                                    'withdraw': self.withdraw_index,
                                }
                                t = datetime.now()
                                time_course_addition_data.add_data(add_data, t=t)
                                start_pumps()
                            elif level >= self.llm.monitor.reference_manager.reference(roi_name).upper:
                                stop_pumps()
                                print('level is above, withdraw')
                                self.withdraw_index += 1
                                withdraw()
                                add_data = {
                                    'dispense': self.dispense_index,
                                    'withdraw': self.withdraw_index,
                                }
                                t = datetime.now()
                                time_course_addition_data.add_data(add_data, t=t)
                                start_pumps()

        def start(self):
            self.llm.start_monitoring()
            time.sleep(15)
            start_pumps()
            super().start()

        def stop(self) -> None:
            super().stop()
            stop_pumps()
            self.llm.stop_monitoring()

    def pause():
        maintain.pause = True
        stop_pumps()

    def resume():
        maintain.pause = False
        start_pumps()


    maintain = MaintainLevel(llm, 10)
    maintain.start()
    time.sleep(1)
    # get the start time from the data from the liquid level monitor, and set that as the start time for the addition
    # csv file to have the 0 point
    start_time: str = maintain.llm.monitor.data.head(1)[maintain.llm.monitor.time_heading][0]
    add_data = {
        'dispense': 0,
        'withdraw': 0,
    }
    time_course_addition_data.add_data(add_data, t=start_time)

    # todo to end, set a breakpoint at the while line, then when the script is paused used the python console to
    #  set run = False, then continue and this will soon end the run
    run = True
    while run:
        time.sleep(5)

    maintain.stop()
    time.sleep(5)
    stop_pumps()

    add_data = {
        'dispense': maintain.dispense_index,
        'withdraw': maintain.withdraw_index,
    }
    t = datetime.now()
    time_course_addition_data.add_data(add_data, t=t)
    time_course_addition_data.save_csv()

    print('done')







